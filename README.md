# nwutils

下記コマンドの出力を見やすくする。

- `ss -timnp`
- `ip -s -s link`

## Usage

```
sstimp
ipss
```

## Install

```
# ~/.local/bin/ にコマンドをインストールする場合
pip3 install nwutils --extra-index-url https://gitlab.com/api/v4/projects/35138303/packages/pypi/simple

# /usr/local/bin/ にコマンドをインストールする場合
sudo pip3 install nwutils --extra-index-url https://gitlab.com/api/v4/projects/35138303/packages/pypi/simple

# pipパッケージをアップデートする場合(extra-index-urlも必要)
pip3 install -U nwutils --extra-index-url https://gitlab.com/api/v4/projects/35138303/packages/pypi/simple

# 特定のバージョンのパッケージをインストールする場合
pip3 uninstall -y nwutils
pip3 install nwutils==0.1.0 --extra-index-url https://gitlab.com/api/v4/projects/35138303/packages/pypi/simple

# プライベートリポジトリで、アクセスにトークンが必要な場合
pip3 install nwutils --extra-index-url https://__token__:<your_personal_token>@gitlab.com/api/v4/projects/35138303/packages/pypi/simple
```

extra-index-urlを指定するのが面倒な場合

```
mkdir ~/.pip
cat <<EOF > ~/.pip/pip.conf
[global]
extra-index-url =
  https://__token__:<your_personal_token>@gitlab.com/api/v4/projects/35138303/packages/pypi/simple   # トークンが必要な場合
  https://gitlab.com/api/v4/projects/35124181/packages/pypi/simple   # このように複数行かける

[install]
trusted-host = gitlab.com
EOF

pip3 install nwutils==0.1.0
```


## 参考
- https://stackoverflow.com/questions/64099010/how-to-deploy-python-packages-to-gitlab-package-registry-with-poetry
- https://docs.mpcdf.mpg.de/doc/data/gitlab/devop-tutorial.html
- https://www.anapaulagomes.me/2021/04/publishing-your-python-package-in-your-gitlab-package-registry/
- https://docs.gitlab.com/ee/user/packages/pypi_repository/index.html