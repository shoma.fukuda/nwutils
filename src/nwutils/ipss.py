import subprocess
import shlex
import time
import re
import sys
from pyfixedwidths import FixedWidthFormatter
import datetime
import json

import argparse


def pp(obj):
    from pprint import pprint
    pprint(obj, width=180)

class IpssEntryRx:
    pass

class IpssEntryTx:
    pass

class IpssEntry:
    def __init__(self, obj):
        self._raw = obj
        self.parse(obj)

    def __getattr__(self, name):
        if name.find('rx.') == 0:
            rxname = name[3:]
            return getattr(self.rx, rxname)
        elif name.find('tx.') == 0:
            txname = name[3:]
            return getattr(self.tx, txname)
        raise AttributeError(name)

    def parse(self, obj):
        self.ifindex = obj.get('ifindex', None)
        self.ifname = obj.get('ifname', None)
        rx = obj.get('stats64', {}).get('rx', {})
        ipssentryrx = IpssEntryRx()
        ipssentryrx.bytes = rx.get('bytes', None)
        ipssentryrx.packets = rx.get('packets', None)
        ipssentryrx.errors = rx.get('errors', None)
        ipssentryrx.dropped = rx.get('dropped', None)
        ipssentryrx.multicast = rx.get('multicast', None)
        ipssentryrx.over_errors = rx.get('over_errors', None)
        ipssentryrx.length_errors = rx.get('length_errors', None)
        ipssentryrx.crc_errors = rx.get('crc_errors', None)
        ipssentryrx.frame_errors = rx.get('frame_errors', None)
        ipssentryrx.fifo_errors = rx.get('fifo_errors', None)
        ipssentryrx.missed_errors = rx.get('missed_errors', None)
        self.rx = ipssentryrx

        tx = obj.get('stats64', {}).get('tx', {})
        ipssentrytx = IpssEntryTx()
        ipssentrytx.bytes = tx.get('bytes', None)
        ipssentrytx.packets = tx.get('packets', None)
        ipssentrytx.errors = tx.get('errors', None)
        ipssentrytx.dropped = tx.get('dropped', None)
        ipssentrytx.carrier_errors = tx.get('carrier_errors', None)
        ipssentrytx.collisions = tx.get('collisions', None)
        ipssentrytx.aborted_errors = tx.get('aborted_errors', None)
        ipssentrytx.fifo_errors = tx.get('fifo_errors', None)
        ipssentrytx.window_errors = tx.get('window_errors', None)
        ipssentrytx.heartbeat_errors = tx.get('heartbeat_errors', None)
        ipssentrytx.carrier_changes = tx.get('carrier_changes', None)
        self.tx = ipssentrytx


class Ipss:
    @classmethod
    def parse(cls, text):
        return cls(text=text)

    def __init__(self, text=None) -> None:
        if not text:
            option = ""
            text = subprocess.check_output(shlex.split(f"ip -s -s -j link")).decode('utf-8')
            self._raw = text

        self.entries = self._parse(text=text)

    def _parse(self, text):
        raw_entries = json.loads(text)
        entries = []
        for entry in raw_entries:
            entries.append(IpssEntry(entry))

        return entries


    def __getitem__(self, idx):
        return self.entries[idx]

    def __iter__(self):
        return iter(self.entries)

    def __len__(self):
        return len(self.entries)

def output_ipss(keys, ipss):
    listofdict = []

    for entry in ipss:
        try:
            listofdict.append({ key: getattr(entry, key) for key in keys})
        except AttributeError as e:
            print(f"key '{e}' not found")


    if 'localaddr' in listofdict[0] and 'peeraddr' in listofdict[0]:
        listofdict = sorted(listofdict, key=lambda entry: entry['peeraddr'] + entry['localaddr'])

    fw = FixedWidthFormatter()
    lines = fw.from_dict(listofdict).to_text(padding=1)
    print(''.join(lines))


def command_main(args):
    keys = args.key.strip().split(',')
    ifnames = args.ifnames

    while True:
        ipss = Ipss()
        print(datetime.datetime.now().isoformat())
        if ifnames:
            ipss = list(filter(lambda entry: entry.ifname in ifnames, ipss))

        output_ipss(keys, ipss)
        time.sleep(1)

def main():
    # コマンドラインパーサーを作成
    parser = argparse.ArgumentParser(description='Fake git command')
    parser.add_argument('-k', '--key', help='keys', default='ifname,rx.bytes,rx.packets,rx.errors,rx.dropped,rx.over_errors,rx.multicast,rx.length_errors,rx.crc_errors,rx.frame_errors,rx.fifo_errors,rx.missed_errors,tx.bytes,tx.packets,tx.errors,tx.dropped')
    # parser.add_argument('-k', '--key', help='keys', default='ifname,rx.bytes,rx.packets,rx.errors,rx.dropped,rx.over_errors,rx.multicast,rx.length_errors,rx.crc_errors,rx.frame_errors,rx.fifo_errors,rx.missed_errors,tx.bytes')
    parser.add_argument('-f', '--filter', default=None)
    parser.add_argument('ifnames', metavar='NAME', nargs='*')
    parser.set_defaults(handler=command_main)

    # コマンドライン引数をパースして対応するハンドラ関数を実行
    args = parser.parse_args()
    if hasattr(args, 'handler'):
        args.handler(args)
    else:
        # 未知のサブコマンドの場合はヘルプを表示
        parser.print_help()


if __name__ == "__main__":
    main()
