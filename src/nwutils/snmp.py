import argparse
from pyfixedwidths import FixedWidthFormatter
import datetime
import time
from pprint import pprint as pp

class Entry:
    pass

class Snmp():
    @classmethod
    def parse(cls, text):
        snmp = cls(pid=None)
        snmp._parse(text)
        return snmp

    def __init__(self, pid=None):
        if pid:
            self._load(pid=pid)

    def _load(self, pid):
        if pid == 'net':
            filename = f"/proc/{pid}/snmp"
        else:
            filename = f"/proc/{pid}/net/snmp"

        with open(filename, "r") as f:
            text = f.read()

        self._parse(text)

    def _parse(self, text):
        lines = text.strip().splitlines()
        for i in range(0, len(lines), 2):
            key_line = lines[i].strip()
            keys = key_line.split()
            data_line = lines[i + 1].strip()
            datas = data_line.split()

            prot = keys.pop(0).replace(':', '')
            _ = datas.pop(0)

            items = {keys[i]: datas[i] for i in range(0, len(keys))}
            entry = Entry()
            for key, val in items.items():
                setattr(entry, key, int(val))
            setattr(self, prot, entry)

def output_snmp(keys, snmp):
    itemdict = {}
    try:
        for key in keys:
            itemdict[key] = getattr(snmp, key)
    except AttributeError as e:
        print(f"key '{e}' not found")

    fw = FixedWidthFormatter()
    lines = fw.from_dict(listofdict).to_text(padding=1)
    print(''.join(lines))

def command_main(args):
    keys = args.key.strip().split(',')
    pid = args.pid
    while True:
        snmp = Snmp(pid=pid)
        print(datetime.datetime.now().isoformat())

        output_snmp(keys, snmp)
        time.sleep(1)

def main():
    print("Implementing.....")
    exit()
    # コマンドラインパーサーを作成
    parser = argparse.ArgumentParser(description='Fake git command')
    parser.add_argument('-k', '--key', help='keys', default='Ip.Forwarding,Ip.InReceives,Ip.InHdrErrors,Ip.InAddrErrors,Ip.ForwDatagrams,Ip.InUnknownProtos,.IpInDiscards')
    parser.add_argument('pid', nargs='?', default='net')
    parser.set_defaults(handler=command_main)

    # コマンドライン引数をパースして対応するハンドラ関数を実行
    args = parser.parse_args()
    if hasattr(args, 'handler'):
        args.handler(args)
    else:
        # 未知のサブコマンドの場合はヘルプを表示
        parser.print_help()


if __name__ == "__main__":
    main()
