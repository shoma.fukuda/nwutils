import pytest
from nwutils.ipss import (Ipss, IpssEntry)
import json

sample1 = [
  {
    "ifindex": 1,
    "ifname": "lo",
    "flags": [
      "LOOPBACK",
      "UP",
      "LOWER_UP"
    ],
    "mtu": 65536,
    "qdisc": "noqueue",
    "operstate": "UNKNOWN",
    "linkmode": "DEFAULT",
    "group": "default",
    "txqlen": 1000,
    "link_type": "loopback",
    "address": "00:00:00:00:00:00",
    "broadcast": "00:00:00:00:00:00",
    "stats64": {
      "rx": {
        "bytes": 181665808,
        "packets": 422872,
        "errors": 0,
        "dropped": 0,
        "over_errors": 0,
        "multicast": 0,
        "length_errors": 0,
        "crc_errors": 0,
        "frame_errors": 0,
        "fifo_errors": 0,
        "missed_errors": 0
      },
      "tx": {
        "bytes": 181665808,
        "packets": 422872,
        "errors": 0,
        "dropped": 0,
        "carrier_errors": 0,
        "collisions": 0,
        "aborted_errors": 0,
        "fifo_errors": 0,
        "window_errors": 0,
        "heartbeat_errors": 0,
        "carrier_changes": 0
      }
    }
  },
  {
    "ifindex": 2,
    "ifname": "ens33",
    "flags": [
      "BROADCAST",
      "MULTICAST",
      "UP",
      "LOWER_UP"
    ],
    "mtu": 1500,
    "qdisc": "fq_codel",
    "operstate": "UP",
    "linkmode": "DEFAULT",
    "group": "default",
    "txqlen": 1000,
    "link_type": "ether",
    "address": "00:0c:29:2f:89:d5",
    "broadcast": "ff:ff:ff:ff:ff:ff",
    "stats64": {
      "rx": {
        "bytes": 1980930649,
        "packets": 1641685,
        "errors": 0,
        "dropped": 0,
        "over_errors": 0,
        "multicast": 0,
        "length_errors": 0,
        "crc_errors": 0,
        "frame_errors": 0,
        "fifo_errors": 0,
        "missed_errors": 0
      },
      "tx": {
        "bytes": 192652930,
        "packets": 535502,
        "errors": 0,
        "dropped": 0,
        "carrier_errors": 0,
        "collisions": 0,
        "aborted_errors": 0,
        "fifo_errors": 0,
        "window_errors": 0,
        "heartbeat_errors": 0,
        "carrier_changes": 104
      }
    },
    "altnames": [
      "enp2s1"
    ]
  },
  {
    "ifindex": 3,
    "ifname": "ens38",
    "flags": [
      "BROADCAST",
      "MULTICAST",
      "UP",
      "LOWER_UP"
    ],
    "mtu": 1500,
    "qdisc": "fq_codel",
    "operstate": "UP",
    "linkmode": "DEFAULT",
    "group": "default",
    "txqlen": 1000,
    "link_type": "ether",
    "address": "00:0c:29:2f:89:df",
    "broadcast": "ff:ff:ff:ff:ff:ff",
    "stats64": {
      "rx": {
        "bytes": 386291,
        "packets": 2316,
        "errors": 0,
        "dropped": 0,
        "over_errors": 0,
        "multicast": 0,
        "length_errors": 0,
        "crc_errors": 0,
        "frame_errors": 0,
        "fifo_errors": 0,
        "missed_errors": 0
      },
      "tx": {
        "bytes": 448081,
        "packets": 5565,
        "errors": 0,
        "dropped": 0,
        "carrier_errors": 0,
        "collisions": 0,
        "aborted_errors": 0,
        "fifo_errors": 0,
        "window_errors": 0,
        "heartbeat_errors": 0,
        "carrier_changes": 8
      }
    },
    "altnames": [
      "enp2s6"
    ]
  },
  {
    "ifindex": 5,
    "ifname": "docker0",
    "flags": [
      "NO-CARRIER",
      "BROADCAST",
      "MULTICAST",
      "UP"
    ],
    "mtu": 1500,
    "qdisc": "noqueue",
    "operstate": "DOWN",
    "linkmode": "DEFAULT",
    "group": "default",
    "link_type": "ether",
    "address": "02:42:be:eb:b4:d8",
    "broadcast": "ff:ff:ff:ff:ff:ff",
    "stats64": {
      "rx": {
        "bytes": 0,
        "packets": 0,
        "errors": 0,
        "dropped": 0,
        "over_errors": 0,
        "multicast": 0,
        "length_errors": 0,
        "crc_errors": 0,
        "frame_errors": 0,
        "fifo_errors": 0,
        "missed_errors": 0
      },
      "tx": {
        "bytes": 6306,
        "packets": 96,
        "errors": 0,
        "dropped": 0,
        "carrier_errors": 0,
        "collisions": 0,
        "aborted_errors": 0,
        "fifo_errors": 0,
        "window_errors": 0,
        "heartbeat_errors": 0,
        "carrier_changes": 23
      }
    }
  }
]

sample_entry1 =  {
    "ifindex": 1,
    "ifname": "lo",
    "flags": [
      "LOOPBACK",
      "UP",
      "LOWER_UP"
    ],
    "mtu": 65536,
    "qdisc": "noqueue",
    "operstate": "UNKNOWN",
    "linkmode": "DEFAULT",
    "group": "default",
    "txqlen": 1000,
    "link_type": "loopback",
    "address": "00:00:00:00:00:00",
    "broadcast": "00:00:00:00:00:00",
    "stats64": {
      "rx": {
        "bytes": 181665808,
        "packets": 422872,
        "errors": 0,
        "dropped": 0,
        "over_errors": 0,
        "multicast": 0,
        "length_errors": 0,
        "crc_errors": 0,
        "frame_errors": 0,
        "fifo_errors": 0,
        "missed_errors": 0
      },
      "tx": {
        "bytes": 181665808,
        "packets": 422872,
        "errors": 0,
        "dropped": 0,
        "carrier_errors": 0,
        "collisions": 0,
        "aborted_errors": 0,
        "fifo_errors": 0,
        "window_errors": 0,
        "heartbeat_errors": 0,
        "carrier_changes": 0
      }
    }
  }

class TestIpss():
    def test_parse(self):
        ipss = Ipss.parse(json.dumps(sample1))
        assert len(ipss) == 4
        assert ipss[0].ifname == "lo"
        assert ipss[1].rx.bytes == 1980930649



class TestIpssEntry():
    def test_parse(self):
        ipssentry = IpssEntry(sample_entry1)
        assert ipssentry.ifindex == 1
        assert ipssentry.ifname == "lo"
        # assert ipssentry.mtu == 65536
        assert ipssentry.rx.bytes == 181665808
        assert ipssentry.rx.length_errors == 0
        assert ipssentry.tx.bytes == 181665808
        assert ipssentry.tx.aborted_errors == 0
