import pytest
from nwutils.sstimp import (Sstimp, SstimpEntry, Skmem)

sample1 = """State                                  Recv-Q                                  Send-Q                                                                     Local Address:Port                                                                     Peer Address:Port                                      Process
ESTAB                                  0                                       0                                                                              127.0.0.1:48928                                                                       127.0.0.1:39783                                      users:(("sshd",pid=98087,fd=11))
         skmem:(r0,rb2620380,t0,tb2626560,f0,w0,o0,bl0,d0) cubic wscale:7,7 rto:208 rtt:5.785/10.476 ato:40 mss:65483 pmtu:65535 rcvmss:18098 advmss:65483 cwnd:10 bytes_sent:550722 bytes_retrans:319 bytes_acked:550404 bytes_received:242754 segs_out:834 segs_in:785 data_segs_out:517 data_segs_in:372 send 905.6Mbps lastsnd:40976 lastrcv:43724 lastack:40976 pacing_rate 1811.0Mbps delivery_rate 52386.4Mbps delivered:518 app_limited busy:3152ms rwnd_limited:4ms(0.1%) retrans:0/4 dsack_dups:4 rcv_rtt:7263.32 rcv_space:69303 rcv_ssthresh:1309330 minrtt:0.04
ESTAB                                  0                                       0                                                                              127.0.0.1:39783                                                                       127.0.0.1:48928                                      users:(("node",pid=98140,fd=19))
         skmem:(r0,rb3799551,t0,tb2626560,f4096,w0,o0,bl0,d4) cubic wscale:7,7 rto:204 rtt:0.384/0.62 ato:40 mss:65483 pmtu:65535 rcvmss:32768 advmss:65483 cwnd:10 bytes_sent:242754 bytes_acked:242754 bytes_received:550403 segs_out:784 segs_in:834 data_segs_out:372 data_segs_in:517 send 13642.3Mbps lastsnd:43724 lastrcv:40976 lastack:40976 pacing_rate 27275.7Mbps delivery_rate 10913.8Mbps delivered:373 app_limited busy:960ms rcv_rtt:0.853 rcv_space:104448 rcv_ssthresh:1898401 minrtt:0.051
ESTAB                                  0                                       0                                                                              127.0.0.1:48968                                                                       127.0.0.1:39783                                      users:(("sshd",pid=101830,fd=10))
         skmem:(r0,rb6291456,t0,tb2626560,f4096,w0,o0,bl0,d0) cubic wscale:7,7 rto:204 rtt:3.624/0.342 ato:40 mss:32768 pmtu:65535 rcvmss:65483 advmss:65483 cwnd:10 bytes_sent:149811 bytes_acked:149812 bytes_received:3534388 segs_out:7445 segs_in:6063 data_segs_out:3299 data_segs_in:5022 send 723.4Mbps lastsnd:44 lastrcv:44 lastack:44 pacing_rate 1446.4Mbps delivery_rate 12483.0Mbps delivered:3300 app_limited busy:14988ms rcv_rtt:44.25 rcv_space:220119 rcv_ssthresh:3144360 minrtt:0.021
"""

sample_entry1 = """ESTAB                                  0                                       0                                                                              127.0.0.1:48928                                                                       127.0.0.1:39783                                      users:(("sshd",pid=98087,fd=11))
         skmem:(r0,rb2620380,t0,tb2626560,f0,w0,o0,bl0,d0) cubic wscale:7,7 rto:208 rtt:5.785/10.476 ato:40 mss:65483 pmtu:65535 rcvmss:18098 advmss:65483 cwnd:10 bytes_sent:550722 bytes_retrans:319 bytes_acked:550404 bytes_received:242754 segs_out:834 segs_in:785 data_segs_out:517 data_segs_in:372 send 905.6Mbps lastsnd:40976 lastrcv:43724 lastack:40976 pacing_rate 1811.0Mbps delivery_rate 52386.4Mbps delivered:518 app_limited busy:3152ms rwnd_limited:4ms(0.1%) retrans:0/4 dsack_dups:4 rcv_rtt:7263.32 rcv_space:69303 rcv_ssthresh:1309330 minrtt:0.04
"""

class Tes1tSstimp():
    def test_init(self):
        sstimp = Sstimp()
        assert len(sstimp) != 0

    def test_parse(self):
        sstimp = Sstimp.parse(sample1)
        assert len(sstimp) == 3
        assert sstimp[0].skmem.rb == 2620380
        assert sstimp[1].skmem.rb == 3799551
        assert sstimp[2].skmem.rb == 6291456


class TestSstimpEntry():
    def test_parse(self):
        sstimpentry = SstimpEntry(sample_entry1)
        assert sstimpentry.state == "ESTAB"
        assert sstimpentry.recvq == "0"
        assert sstimpentry.sendq == "0"
        assert sstimpentry.localaddr == "127.0.0.1:48928"
        assert sstimpentry.peeraddr == "127.0.0.1:39783"
        assert sstimpentry.process == 'users:(("sshd",pid=98087,fd=11))'
        assert sstimpentry.skmem.r == 0
        assert sstimpentry.skmem.rb == 2620380
        assert sstimpentry.skmem.t == 0
        assert sstimpentry.skmem.tb == 2626560
        assert sstimpentry.skmem.f == 0
        assert sstimpentry.skmem.w == 0
        assert sstimpentry.skmem.o == 0
        assert sstimpentry.skmem.bl == 0
        assert sstimpentry.skmem.d == 0
        assert sstimpentry.algo == 'cubic'
        assert sstimpentry.wscale == '7,7'
        assert sstimpentry.rto == '208'
        assert sstimpentry.rtt == '5.785/10.476'
        assert sstimpentry.ato == '40'
        assert sstimpentry.mss == '65483'
        assert sstimpentry.pmtu == '65535'
        assert sstimpentry.rcvmss == '18098'
        assert sstimpentry.advmss == '65483'
        assert sstimpentry.cwnd == '10'
        assert sstimpentry.bytes_sent == '550722'
        assert sstimpentry.bytes_retrans == '319'
        assert sstimpentry.bytes_acked == '550404'
        assert sstimpentry.bytes_received == '242754'
        assert sstimpentry.segs_out == '834'
        assert sstimpentry.segs_in == '785'
        assert sstimpentry.data_segs_out == '517'
        assert sstimpentry.data_segs_in == '372'
        assert sstimpentry.send == '905.6Mbps'
        assert sstimpentry.lastsnd == '40976'
        assert sstimpentry.lastrcv == '43724'
        assert sstimpentry.lastack == '40976'
        assert sstimpentry.pacing_rate == '1811.0Mbps'
        assert sstimpentry.delivery_rate == '52386.4Mbps'
        assert sstimpentry.delivered == '518'
        assert sstimpentry.busy == '3152ms'
        assert sstimpentry.rwnd_limited == '4ms(0.1%)'
        assert sstimpentry.retrans == '0/4'
        assert sstimpentry.dsack_dups == '4'
        assert sstimpentry.rcv_rtt == '7263.32'
        assert sstimpentry.rcv_space == '69303'
        assert sstimpentry.rcv_ssthresh == '1309330'
        assert sstimpentry.minrtt == '0.04'

class TestSkmem():
    def test_parse1(self):
        skmem = Skmem("skmem:(r0,rb2620380,t0,tb2626560,f0,w0,o0,bl0,d0)")
        assert skmem.r == 0
        assert skmem.rb == 2620380
        assert skmem.t == 0
        assert skmem.tb == 2626560
        assert skmem.f == 0
        assert skmem.w == 0
        assert skmem.o == 0
        assert skmem.bl == 0
        assert skmem.d == 0

    def test_parse1(self):
        skmem = Skmem()
        assert skmem.r == None
        assert skmem.rb == None
        assert skmem.t == None
        assert skmem.tb == None
        assert skmem.f == None
        assert skmem.w == None
        assert skmem.o == None
        assert skmem.bl == None
        assert skmem.d == None
